import React from 'react'

export default function Shoes(props) {
    let {shoes, addShoes} = props
    return (
        <div className="card col-4">
            <img className="card-img-top" style={{height: 300}} src={shoes.hinhAnh} alt='' />
            <div className="card-body">
                <h4 className="card-title">{shoes.tenSP}</h4>
                <p className="card-text">{shoes.giaBan.toLocaleString()} Vnd</p>
                <button onClick={() => addShoes(shoes)} className='btn btn-success'>Thêm giỏ hàng</button>
            </div>
        </div>
    )
}
