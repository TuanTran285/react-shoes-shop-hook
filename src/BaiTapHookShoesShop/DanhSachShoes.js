import React, { useState } from 'react'
import shoesList from '../data/data.json'
import Shoes from './Shoes'
export default function DanhSachShoes(props) {
let {addShoes} = props
    const rendershoesList = () => {
        return shoesList.map((shoes, index) => {
            return  <Shoes shoes={shoes} key={index} addShoes={addShoes}/>
        })
    }
    return (
        <div className='row'>
            {rendershoesList()}
        </div>
    )
}
