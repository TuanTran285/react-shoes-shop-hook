import React from 'react'

export default function GioHangShoes(props) {
    const {shoesCartList, handleQuantity, deleteShoes} = props
    const renderShoesCartList = () => {
        return shoesCartList.map((shoes, index) => {
            return <tr key={index}>
                <td>{shoes.maSP}</td>
                <td><img src={shoes.hinhAnh} style={{width: 80, height: 80, objectFit: 'cover'}} alt="" /></td>
                <td>{shoes.tenSP}</td>
                <td>{shoes.giaBan.toLocaleString()}</td>
                <td>
                    <button className='btn btn-danger' onClick={() => handleQuantity(shoes.maSP, 'sub')}>-</button>
                    <span className='mx-2'>{shoes.soLuong}</span>
                    <button className='btn btn-danger' onClick={() => handleQuantity(shoes.maSP, 'add')}>+</button>
                </td>
                <td>{(shoes.giaBan * shoes.soLuong).toLocaleString()}</td>
                <td><button onClick={() => deleteShoes(shoes.maSP)} className='btn btn-danger'>Xóa</button></td>
            </tr>
        })
    }
    return (
        <div className="modal fade" id="modelId" tabIndex={-1} role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content" style={{width: 800}}>
                    <div className="modal-header">
                        <h5 className="modal-title">Modal title</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div className="modal-body texxt-center">
                        <table className='table text-center'>
                        <thead>
                            <tr>
                                <th>Ma sp</th>
                                <th>Hinh anh</th>
                                <th>Ten spP</th>
                                <th>Gia ban</th>
                                <th>So luong</th>
                                <th>Tổng tiền</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>{renderShoesCartList()}</tbody>
                        </table>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>

    )
}
