import React, { useState } from 'react'
import DanhSachShoes from './DanhSachShoes'
import GioHangShoes from './GioHangShoes'

export default function BaiTapHookShoesShop() {
  let [shoesCart, setShoesCart] = useState([])
  function addShoes(shoes) {
    let index = shoesCart.findIndex(item => {
      return item.maSP === shoes.maSP 
    })
    if(index !== -1) {
      setShoesCart([...shoesCart], shoesCart[index].soLuong+=1)
    }else {
      setShoesCart([...shoesCart, {...shoes, soLuong: 1}])
    }
  }
  function handleQuantity(maSP, type) {
    let index = shoesCart.findIndex(item => {
      return item.maSP === maSP
    })
    if(type === 'add') {
      setShoesCart([...shoesCart], shoesCart[index].soLuong += 1)
    }else {
      setShoesCart([...shoesCart], shoesCart[index].soLuong -= 1)
      if(shoesCart[index].soLuong <= 0) {
        shoesCart.splice(index, 1)
        setShoesCart([...shoesCart])
      }
    }
  }
  function deleteShoes(maSP) {
    let newShoesCart = shoesCart.filter(shoes => shoes.maSP !== maSP)
    setShoesCart([...newShoesCart])
  }
  const totalQuantiTy = shoesCart.reduce((acc, curr) => {
    return acc+= curr.soLuong
  }, 0)
  return (
    <div className='container'>
    <h1 className='text-danger text-center mb-4'>Bài tập shoes shop</h1>
        <div className='text-right' style={{fontSize: 24}} data-toggle="modal" data-target="#modelId">
        <i className="fa-solid fa-cart-shopping"></i>
        <span>{totalQuantiTy}</span>
        </div>
        <GioHangShoes shoesCartList={shoesCart}  handleQuantity={handleQuantity} deleteShoes={deleteShoes}/>
        <DanhSachShoes addShoes={addShoes}/>
    </div>
  )
}
